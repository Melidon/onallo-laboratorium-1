print("Loading data from files...")

import pandas as pd

food_data = pd.read_csv("./data/train.csv").set_index("id").groupby(["center_id", "meal_id"])

print("Dealing with missing data...")

import numpy as np

def transform(food_data):
    data_frames = []
    for (center_id, meal_id), data_frame in food_data:
        if data_frame.shape[0] != 145:
            all_weeks = pd.DataFrame({'week': range(1, 145+1)})
            data_frame = data_frame.merge(all_weeks, on='week', how='right')
            data_frame['center_id'] = center_id
            data_frame['meal_id'] = meal_id
        else:
            data_frames.append(data_frame)
    return pd.concat(data_frames).groupby(["center_id", "meal_id"])

food_data = transform(food_data)

# Just to make sure
for ((center_id, meal_id), data_frame) in food_data:
    if data_frame.shape[0] != 145:
        print("This should not happen.")
        quit()

center_data = pd.read_csv("./data/fulfilment_center_info.csv").set_index("center_id")
center_type_dict = { center: i+1 for i, center in enumerate(center_data["center_type"].unique()) }
center_data["center_type"] = center_data["center_type"].map(center_type_dict)

meal_data = pd.read_csv("./data/meal_info.csv").set_index("meal_id")
category_dict = { category: i+1 for i, category in enumerate(meal_data["category"].unique()) }
cuisine_dict = { cuisine: i+1 for i, cuisine in enumerate(meal_data["cuisine"].unique()) }
meal_data["category"] = meal_data["category"].map(category_dict)
meal_data["cuisine"] = meal_data["cuisine"].map(cuisine_dict)

length = len(next(iter(food_data))[1])
prediction_length = 45
context_length = length - prediction_length
freq = "W"
start = pd.Period("2021-01-01", freq=freq)

target = "num_orders"
feat_static_cat_center = ["city_code", "region_code", "center_type"]
feat_static_cat_meal = ["category", "cuisine"]
feat_static_real_center = ["op_area"]
feat_static_real_meal = []
feat_dynamic_cat = ["emailer_for_promotion", "homepage_featured"]
feat_dynamic_real = ["checkout_price", "base_price"]

print("Creating dataset...")

import numpy as np
np.bool = bool

def feat_static_cat(center_id, meal_id):
    center_static_cat = center_data.loc[center_id][feat_static_cat_center].to_numpy()
    meal_static_cat = meal_data.loc[meal_id][feat_static_cat_meal].to_numpy()
    result = np.concatenate((center_static_cat, meal_static_cat))
    return result.astype(int)

def feat_static_real(center_id, meal_id):
    center_static_real = center_data.loc[center_id][feat_static_real_center].to_numpy()
    meal_static_real = meal_data.loc[meal_id][feat_static_real_meal].to_numpy()
    result = np.concatenate((center_static_real, meal_static_real))
    return result

from gluonts.dataset.common import ListDataset
from gluonts.dataset.field_names import FieldName
from gluonts.transform import AddObservedValuesIndicator

transformation = AddObservedValuesIndicator(
    target_field=FieldName.TARGET,
    output_field=FieldName.OBSERVED_VALUES,
)

train_ds = ListDataset(
    [
        {
            FieldName.TARGET: data_frame[target].to_numpy()[:context_length],
            FieldName.START: start,
            FieldName.FEAT_STATIC_CAT: feat_static_cat(center_id, meal_id)[:context_length],
            FieldName.FEAT_STATIC_REAL: feat_static_real(center_id, meal_id)[:context_length],
            FieldName.FEAT_DYNAMIC_CAT: data_frame[feat_dynamic_cat].to_numpy()[:context_length].T,
            FieldName.FEAT_DYNAMIC_REAL: data_frame[feat_dynamic_real].to_numpy()[:context_length].T,
        }
        for (center_id, meal_id), data_frame in food_data
    ],
    freq=freq,
)

train_tf = transformation(iter(train_ds), is_train=True)
train_ds = list(train_tf)

test_ds = ListDataset(
    [
        {
            FieldName.TARGET: data_frame[target].to_numpy(),
            FieldName.START: start,
            FieldName.FEAT_STATIC_CAT: feat_static_cat(center_id, meal_id),
            FieldName.FEAT_STATIC_REAL: feat_static_real(center_id, meal_id),
            FieldName.FEAT_DYNAMIC_CAT: data_frame[feat_dynamic_cat].to_numpy().T,
            FieldName.FEAT_DYNAMIC_REAL: data_frame[feat_dynamic_real].to_numpy().T,
        }
        for (center_id, meal_id), data_frame in food_data
    ],
    freq=freq,
)

test_tf = transformation(iter(test_ds), is_train=False)
test_ds = list(test_tf)

from pathlib import Path

path = Path("./model/")

if (True):

    print("Training model...")

    from gluonts.mx import DeepAREstimator, Trainer

    estimator = DeepAREstimator(
        freq=freq,
        prediction_length=prediction_length,
        trainer=Trainer(
            epochs = 8,
            num_batches_per_epoch = 128,
            learning_rate = 1e-3,
        ),
        context_length=context_length,
        num_layers = 8,
        dropout_rate = 0.25,
        use_feat_dynamic_real = True,
        use_feat_static_cat = False,
        use_feat_static_real = True,
        batch_size = 128,
    )

    predictor = estimator.train(train_ds)

    print("Saving model...")

    predictor.serialize(path)
else:
    print("Loading model...")

    from gluonts.model.predictor import Predictor

    predictor = Predictor.deserialize(path)

print("Making predictions...")

from gluonts.evaluation import make_evaluation_predictions

forecast_it, ts_it = make_evaluation_predictions(
    dataset=test_ds,
    predictor=predictor,
    num_samples=100,
)

forecasts = list(forecast_it)
tss = list(ts_it)

print("Evaluating...")

from gluonts.evaluation import Evaluator

evaluator = Evaluator(quantiles=[0.1, 0.5, 0.9])
agg_metrics, item_metrics = evaluator(tss, forecasts)

import json

print(json.dumps(agg_metrics, indent=4))

print("Plotting the first forecast...")

import matplotlib.pyplot as plt

def plot_prob_forecasts(i, ts_entry, forecast_entry):
    plot_length = 145
    prediction_intervals = (50.0, 90.0)
    legend = ["observations", "median prediction"] + [
        f"{k}% prediction interval" for k in prediction_intervals
    ][::-1]

    fig, ax = plt.subplots(1, 1, figsize=(10, 7))
    ts_entry[-plot_length:].plot(ax=ax)
    forecast_entry.plot(prediction_intervals=prediction_intervals, color="g")
    plt.grid(which="both")
    plt.legend(legend, loc="upper left")
    plt.savefig(f"plots/plot_{i}.png")  # Save the figure as an image
    plt.close(fig)  # Close the figure to free up resources

for i, (ts_entry, forecast_entry) in enumerate(zip(tss, forecasts)):
    plot_prob_forecasts(i, ts_entry, forecast_entry)
